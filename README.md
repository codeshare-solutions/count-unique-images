You are trying to count the number of unique images you have shown to each demographic.

Example:

Input:
{
under_20: ['Bike', 'Pokemon', 'Phone'],
20_39: ['Phone', 'Bike'],
40_59: ['Phone', 'Cruise', 'Lawn Mower']
}

Output:
{
under_20: 1,
20_39: 0,
40_59: 2
}
