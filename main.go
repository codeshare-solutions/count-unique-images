package main

import (
	"fmt"
)

func countUniqueImages(demographics map[string][]string) map[string]int {
	// convert lists to sets for each demographic group, we could just use demographics directly but we do not want to mutate the input
	// also sets inherently handle uniqueness, so any duplicates within a group are automatically managed
	groups := make(map[string]map[string]struct{})
	for k, v := range demographics {
		set := make(map[string]struct{})
		for _, item := range v {
			set[item] = struct{}{}
		}
		groups[k] = set
	}

	// initialize a map to store the unique count for each group
	uniqueCount := make(map[string]int)

	// find unique images for each demographic
	for group := range groups {
		uniqueImages := make(map[string]struct{})
		for image := range groups[group] {
			uniqueImages[image] = struct{}{}
		}

		// delete the images shown to other groups
		for otherGroup := range groups {
			if group != otherGroup {
				for image := range groups[otherGroup] {
					delete(uniqueImages, image)
				}
			}
		}

		//the number of unique images is the size of the remaining set
		uniqueCount[group] = len(uniqueImages)
	}

	return uniqueCount
}

func main() {
	inputData := map[string][]string{
		"under_20": {"Bike", "Pokemon", "Phone"},
		"20_39":    {"Phone", "Bike"},
		"40_59":    {"Phone", "Cruise", "Lawn Mower"},
	}

	outputData := countUniqueImages(inputData)
	fmt.Println(outputData)
}